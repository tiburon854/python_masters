"""
Простые арифметические операции.
"""


a = 60
b = 4

sum_result = a + b
difference_result = a - b
product_result = a * b
quotient_result = a / b

print("Сумма:", sum_result)
print("Разность:", difference_result)
print("Произведение:", product_result)
print("Деление:", quotient_result)
