class BaseLoc:
    page_header = 'h1'


class SignIn(BaseLoc):
    email = '#email'
    password = '#pass'
    sign_in_button = '#send2'
    alert = '[role="alert"]'


class WhatsNew(BaseLoc):
    button = ''
